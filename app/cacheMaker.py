import json
from werkzeug.contrib.cache import SimpleCache
from databaseFunc import *

session = Session()
cache = SimpleCache()

def generate_data(acquisition_era_name, dataTier):
    listDatasetsRaw = listDatasetsRAW(acquisition_era_name)
    for datasetRaw in listDatasetsRaw:
        if datasetRaw.isParent:
            for children in datasetRaw.listChildrens(dataTier = dataTier):
                if datasetRaw.listChildrens(dataTier = dataTier): #non empty
                    print datasetRaw.datasetName    
                    file_name = "%s-%s-%s.json" %(children.primaryDatasetName, children.acquisition_era_name, children.processed_ds_name)
                    return  generate_cache(file_name, children, datasetRaw)

def generate_data_children(acquisition_era_name, dataTier, fileName):
    listDatasetsRaw = listDatasetsRAW(acquisition_era_name)
    for datasetRaw in listDatasetsRaw:
        if datasetRaw.isParent:
            if datasetRaw.listChildrens(dataTier = dataTier): #non empty
                for children in datasetRaw.listChildrens(dataTier = dataTier):
                    file_name = "%s-%s-%s.json" %(children.primaryDatasetName, children.acquisition_era_name, children.processed_ds_name)
                    if file_name == fileName:
                        return  generate_cache(file_name, children, datasetRaw)


def generate_cache(file_name, children, datasetRaw):
    cache_file = cache.get(file_name) 
    print cache_file
    if cache_file is None:
        data = []
        for runContent in children.listRunContents():
            nEChildren = runContent.numberEvents
            nEParent = datasetRaw.runContent(runContent.runNumber).numberEvents
            ratio = 0.0
            nEParent_float = float(nEParent)
            if nEParent_float > 0:
                ratio = round(float(nEChildren)/nEParent_float,3)
            data.append({'runNumber': runContent.runNumber, 'events': nEChildren, 'eventsParents': nEParent, 'ratio' : "%0.3f" %ratio})
        data_to_cache = json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))  
        cache.set(file_name, data_to_cache, timeout= 60) 
        cache_file = cache.get(file_name)
    return cache_file

def generate_children(acquisition_era_name, dataTier):
    print "###DEBUG15"
    listTest_sqlite = listDatasetsRAW(acquisition_era_name)
    DicoTest_json = []
    print "###DEBUG2### %s, %s" % (len(listTest_sqlite), dataTier) ##works > 0
    for dataset in listTest_sqlite:
        DicoTest = {}
        listTest = {}
        tempDico = []
        if dataset.isParent:
            print "\t 1st IF"
            if dataset.listChildrens(dataTier = dataTier): #non empty
                print "\t\t 2nd IF"
                DicoTest['parentPrimaryDatasetName'] = dataset.primaryDatasetName
                DicoTest['parentDatasetName'] = dataset.datasetName
                for children in dataset.listChildrens(dataTier = dataTier):
                    print "\t\t\t 3rd FOR"
                    name = children.processed_ds_name.split('-')
                    listTest['childrenName'] = name[0]
                    listTest['childrenDatasetName'] = children.datasetName
                    file_name = "%s-%s-%s.json" %(children.primaryDatasetName, children.acquisition_era_name, children.processed_ds_name)
                    listTest['fileName'] = file_name
                    tempDico.append(listTest)
                    listTest = {}
                DicoTest['childrenDatasets'] = sorted(tempDico, key=lambda tempDico:tempDico['childrenName'])
                DicoTest_json.append(DicoTest)
    print "###DEBUG2.5 %s" % (len(DicoTest_json))
    print "###DEBUG2.7 %s" % (len(sorted(DicoTest_json, key=lambda DicoTest_json:DicoTest_json['parentPrimaryDatasetName'])))
    return sorted(DicoTest_json, key=lambda DicoTest_json:DicoTest_json['parentPrimaryDatasetName'])
                
