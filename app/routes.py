import os 
import subprocess
import logging
import json

from app import db
from app import app
from flask import g, jsonify, abort, request, make_response, render_template
from sqlalchemy.exc import IntegrityError
from datetime import datetime
import databaseFunc
from cacheMaker import *
import config
import traceback
from os.path import join

from .models import User

from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

# This app relies on authentication being done in the frontend process via SSO.
# --- GET index

@app.route('/')
@app.route('/index') #Main page
def index():
	gitInfo = run_in_shell('/usr/bin/git describe --all --long', shell = True)
	name = "This is a test"	
	listAcquisitionEra = listAcquisitionEras()
	return render_template('index.html', listAcquisitionEra = listAcquisitionEra)

@app.route('/get_era_data/get_data_tier/<run_name>')
def dataTier(run_name):
	listDataTier = listDataTiers(run_name)
	return render_template('dataTierSlct.html', listDataTier = listDataTier, run_name = run_name)

@app.route('/temp/css')
def css():
	return render_template('PromptCalibMonitoring.css')

@app.route('/get_era_data/get_data_tier/<run_name>/<data_tier>') #skip to the acquistion era choosen (it's called run_name because of Run2015A, but sometimes it's not called run, so probably needs to change the name
def run(run_name, data_tier):
	listGroups = generate_children(run_name, data_tier) #Generate dynamically the dictionnay of the dataset with children with ALCARECO as a dataTier, this will need upgrade when moving to more run/dataTier. It'll probably has to be like url : <acquisition_era>/<dataTier> and thus calling generate_children(acquistion_era,dataTier)
	print "####DEBUG4 %s" % (listGroups)
	return render_template('plots.html', run_name = run_name, listGroups = listGroups, dataTier = data_tier) #return run_name.html with listGroups that can be used with flask

# a simple "echo" method
@app.route('/echo/<string:what>', methods=['GET','POST'])
def echo(what):
	return jsonify( { 'echo' : str(what) } )

# trying to make a method NOT USED 
#@app.route('/plots/<string:acquisitionEra>/', methods=['GET'])
#def plots(acquisitionEra):
#	listTest_sqlite = listDatasetsRAW(acquisitionEra)
#	DicoTest_json = []
#	dataTier = "ALCARECO"
#	for dataset in listTest_sqlite:
#		DicoTest = {}
#		listTest = []
#		if dataset.isParent:
#			if dataset.listChildrens(dataTier = dataTier): #non empty
#				DicoTest['parentDataset'] = dataset.datasetName
#				for children in dataset.listChildrens(dataTier = dataTier):
#					listTest.append(children.datasetName)
#				DicoTest['childrenDatasets'] = listTest
#				DicoTest_json.append(DicoTest)
#	return jsonify( { 'datasets' : DicoTest_json})

#	return render_template('dataset.json')

@app.route('/get_era_data/get_data_tier/<run_name>/<data_tier>/data/readable/<string:fileName>/', methods=['GET'])
def dataFromCache(run_name, data_tier, fileName):
	cache_file = generate_data_children(run_name, data_tier, fileName)	
	cache_file_json = json.loads(cache_file)
	return render_template('dataExposed.html', cache_file = cache_file_json)#return the readable data file, exctract from json cache

@app.route('/get_era_data/get_data_tier/<run_name>/<data_tier>/data/json/<string:fileName>/', methods=['GET'])
def jsonFromCache(run_name, data_tier, fileName):
	cache_file = generate_data_children(run_name, data_tier, fileName)	
	return cache_file #return the json cache file, there it'll be read by the plot function

@app.route('/plot/test', methods=['GET'])# not used
def dataFrom():
	return render_template('test.html')



# --- error handlers

@auth.error_handler
def unauthorized():
	# return 403 instead of 401 to prevent browsers from displaying the default
	# auth dialog
	return make_response(jsonify({'message': 'Unauthorized access'}), 403)

@app.errorhandler(400)
def bad_request(error):
	return make_response(jsonify({'message': 'Bad request'}), 400)

@app.errorhandler(404)
def not_found(error):
	return error404()

@app.errorhandler(409)
def integration_error(error):
	return make_response(jsonify({'message': 'Duplicate entry'}), 409)

@app.errorhandler(500)
def internal_error(error):
	return make_response(jsonify({'message': 'Internal server error'}), 500)

# --- utilities

def success():
	return make_response(jsonify({'success': True}), 200)

def error404():
	return make_response(jsonify({'message': 'Not found'}), 404)

def run_in_shell(*popenargs, **kwargs):
	process = subprocess.Popen(*popenargs, stdout=subprocess.PIPE, **kwargs)
	stdout = process.communicate()[0]
	returnCode = process.returncode
	cmd = kwargs.get('args')
	if cmd is None:
		cmd = popenargs[0]
	if returnCode:
		raise subprocess.CalledProcessError(returnCode, cmd)
	return stdout
