from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, distinct
from sqlalchemy.orm import  backref, sessionmaker, relation


Base = declarative_base()
engine = create_engine('sqlite:////data/services/DatasetMon/dataset_database.sqlite')

class dataset_table(Base):
	__tablename__ = 'dataset'
	id = Column(Integer, primary_key=True)
	datasetName =  Column(String)
	primaryDatasetName = Column(String)
	processed_ds_name = Column(String)
	dataTier = Column(String)
	acquisition_era_name = Column(String)
	isParent = Column(Integer)
	parents_id = Column(Integer, ForeignKey('dataset.id'))
	children = relation("dataset_table")
	parentPrimaryKey = Column(String)
	runContents = relation("runContent_table", order_by="runContent_table.id",backref="dataset_table")


	def listRunContents(self):
		__result = []
		for runContent in self.runContents:
			__result.append(runContent)
		__result = sorted(__result, key = lambda runContent: runContent.runNumber)
		return __result
	
	def listAcquisitionEras(self):# this needs to be changed, only returns acquisition eras  of 2015 (used in index.html)
		__result = []
		i = 0
		for acquisitionEra in self.acquisition_era_name:
			if acquisitionEra[i] !=  acquisitionEra[i+1] and acquisitionEra.startswith("2015"):
				__result.append(acquisitionEra)
		return __result


	def runContent(self, runNumber):
		__result = []
		for runContent in self.runContents:
			if runContent.runNumber == runNumber:
				__result = runContent
		if __result:
			return __result
		else:
			return "This dataset doesn't contain this run number."

	def listChildrens(self, dataTier): #change if needed, only returns v1 processing version
		__result = []
		for children in self.children:
			if children.dataTier == dataTier and children.processed_ds_name.endswith("v1") and not children.processed_ds_name.endswith("v3-v1"):
				__result.append(children)
		return __result


	def __repr__(self):
        	return "<dataset(datasetName='%s', primaryDatasetName='%s', processed_ds_name='%s', dataTier='%s', acquisition_era_name='%s', isParent='%s')>" % (self.datasetName, self.primaryDatasetName, self.processed_ds_name, self.dataTier, self.acquisition_era_name, self.isParent)

class runContent_table(Base):
	__tablename__ = 'runContent'
	id = Column(Integer, primary_key=True)
	runNumber =  Column(String)
	numberEvents = Column(String)
	dataset_table_id = Column(Integer, ForeignKey('dataset.id'))
	lsContents = relation("lsContent_table", order_by="lsContent_table.id",backref="runContent_table")

	def __repr__(self):
        	return "<run(runNumber='%s', numberEvents='%s')>" % (self.runNumber, self.numberEvents)
	

class lsContent_table(Base):
	__tablename__ = 'lsContent'
	id = Column(Integer, primary_key=True)
	lumiSectionNumber =  Column(String)
	numberEvents = Column(String)
	runContent_table_id = Column(Integer, ForeignKey('runContent.id'))

	
	def __repr__(self):
        	return "<lumisection(lumiSectionNumber='%s', numberEvents='%s')>" % (self.lumiSectionNumber, self.numberEvents)


Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)
session = Session()

	

def listPrimaryDatasets(acquisition_era_name):
	__result = []
	for dataset in session.query(dataset_table).filter(dataset_table.acquisition_era_name == acquisition_era_name):
		__result.append(dataset.primaryDatasetName)
	return sorted(list(set(__result)))

def listDatasetsRAW(acquisition_era_name):
	__result = []
	for dataset in session.query(dataset_table).filter(dataset_table.acquisition_era_name == acquisition_era_name).filter(dataset_table.dataTier == "RAW"):
		__result.append(dataset)
	return __result

def listDatasetsALCARECO(acquisition_era_name):
	__result = []
	for dataset in session.query(dataset_table).filter(dataset_table.acquisition_era_name == acquisition_era_name).filter(dataset_table.dataTier == "ALCARECO"):
		__result.append(dataset.datasetName)
	return sorted(list(set(__result)))

def listAcquisitionEras(): # filters out Every acquisition era if not starts with "CMSSW", because was getting AE's like "CMSSW_patch_12.2.3.1 etc", which were not needed at the time
	__result = []
	i = 1
	for element in session.query(dataset_table).all():
		if not element.acquisition_era_name in __result and not element.acquisition_era_name.startswith("CMSSW"):
			__result.append(element.acquisition_era_name)
	return __result

def listDataTiers(run_name):
	__result = []
	i = 1
	for element in session.query(dataset_table).all():
		if not element.dataTier in __result and element.acquisition_era_name == run_name:
			__result.append(element.dataTier)
	return sorted(__result)

















